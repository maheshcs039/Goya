$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'goya'

require 'calabash-cucumber'
require_relative '../features/support/classes'
require_relative '../features/ios/support/v2'

$world = CalabashProxy.new(self)
$world.extend(Calabash::V2)
def calabash; $world end

RSpec.configure do |config|
  #config.run_all_when_everything_filtered = true
  #config.filter_run :focus
  #config.order = 'random'
  config.before do
    launcher = Calabash::Cucumber::Launcher.launcher
    options = {
      # Add launch options here.
    }
    relaunch = true
    no_relaunch = true
    if no_relaunch
      begin
        launcher.ping_app
        attach_options = options.dup
        attach_options[:timeout] = 1
        launcher.attach(attach_options)
        relaunch = launcher.device == nil
      rescue => e
        RunLoop.log_info2("Tag says: don't relaunch, but cannot attach to the app.")
        RunLoop.log_info2("#{e.class}: #{e.message}")
        RunLoop.log_info2("The app probably needs to be launched!")
      end
    end
    launcher.relaunch(options) if relaunch
  end
  config.before(:each) do
    #element_exists('view')
  end
  config.after do
    launcher = Calabash::Cucumber::Launcher.launcher
    if launcher.quit_app_after_scenario?
      calabash_exit
    end
  end
end
=begin
def escape_quotes(str)
  str.gsub("'", "\\\\'")
end
  
#### emulate cucumber reports
def embed(path, type, label)
  p "storing report: #{path} (type: #{type}, label:#{label})"
end
=end
RSpec::Matchers.define :show_view do |expected_mark|
  # match do
  #   (element_exists( "view marked:'#{expected_mark}'" ) or
  #    element_exists( "view text:'#{expected_mark}'"))
  # end
  #
  # failure_message_for_should do
  #   "No visible view marked '#{expected_mark}' found"
  # end
  #
  # failure_message_for_should_not do
  #   "Found visible view marked '#{expected_mark}'"
  # end
end
