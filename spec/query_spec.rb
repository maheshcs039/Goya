require 'spec_helper'

RSpec.describe Query do
  describe '#initialize' do
    it 'creates a new query' do
      expect(Query.new.to_s).to eq('*')
      expect(Query.new('UIView').to_s).to eq('UIView')
      expect(Query.new('UILabel', text: 'Hello').to_s).to eq("UILabel text:'Hello'")
    end
  end
end
