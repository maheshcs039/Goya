# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'goya/version'

Gem::Specification.new do |spec|
  spec.name          = 'goya'
  spec.version       = Goya::VERSION
  spec.authors       = ['Andrew Myasnikov']
  spec.email         = ['andrew.myasnikov@rakuten.com']

  spec.summary       = 'Cucumber from Hell'
  spec.description   = 'Higher level API for Calabash'
  spec.homepage      = 'https://vats.rakuten-it.com'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)
  spec.metadata['allowed_push_host'] = 'https://vats.rakuten-it.com'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'guard'
  spec.add_development_dependency 'guard-livereload'
  spec.add_development_dependency 'guard-yard'
  spec.add_development_dependency 'geminabox'
  spec.add_dependency 'activeresource'
  spec.add_dependency 'concurrent-ruby'
  spec.add_dependency 'calabash-android'
  spec.add_dependency 'calabash-cucumber'
  spec.add_dependency 'rest-client'
  spec.add_dependency 'rspec-expectations'
  spec.add_dependency 'versionomy'
end
