package sh.calaba.instrumentationbackend;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Bridge {
    private static Field getField(Class<?> cls, String name) {
        if (cls == null) {
            return null;
        }
        for (Field field: cls.getDeclaredFields()) {
            if (field.getName().equals(name)) {
                return field;
            }
        }
        return getField(cls.getSuperclass(), name);
    }
    private static Method getMethod(Class<?> cls, String name) {
        if (cls == null) {
            return null;
        }
        for (Method method: cls.getDeclaredMethods()) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return getMethod(cls.getSuperclass(), name);
    }
    public static String eval(String exp) throws Exception {
        String[] path = exp.split("\\.");
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (String name: path) {
            if (sb.length() != 0) sb.append('.');
            sb.append(name);
            count++;
            char c = name.charAt(0);
            if (c == Character.toUpperCase(c)) break;
        }
        String root = sb.toString();
        String[] newPath = new String[path.length - count];
        System.arraycopy(path, count, newPath, 0, newPath.length);
        path = newPath;
        Object object = Class.forName(root);
        for (String name: path) {
            Class cls = object instanceof Class ? (Class)object : object.getClass();
            if (name.endsWith(")")) {
                String[] args = name.substring(name.indexOf('(') + 1, name.length() - 1).split(",");
                if (args.length == 1) {
                    args = new String[] {};
                }
                name = name.substring(0, name.indexOf('('));
                Method method = getMethod(cls, name);
                if (method == null && object instanceof Class) {
                    method = getMethod(object.getClass(), name);
                }
                if (method != null) {
                    object = method.invoke(object, (Object[])args);
                } else {
                    throw new RuntimeException("Method not found: " + name + " object: " + object.toString());
                }
            } else {
                Field field = getField(cls, name);
                if (field != null) {
                    object = field.get(object);
                } else {
                    throw new RuntimeException("Field not found: " + name + " object: " + object.toString());
                }
            }
        }
        if (object == null) {
            return "null";
        }
        if (object instanceof Object[]) {
            return Arrays.toString((Object[])object);
        }
        return object.toString();
    }
}
