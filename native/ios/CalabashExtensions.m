
@interface UIScrollView(Calabash)

- (void)scrollToX:(CGFloat)x;
- (void)scrollToY:(CGFloat)y;

@end

@implementation UIScrollView(Calabash)

- (void)scrollToX:(CGFloat)x {
    self.contentOffset = CGPointMake(x, self.contentOffset.y);
}

- (void)scrollToY:(CGFloat)y {
    self.contentOffset = CGPointMake(self.contentOffset.x, y);
}

@end

@interface UITabBar(Calabash)

@property (nonatomic, readonly) NSUInteger selectedIndex;

@end

@implementation UITabBar(Calabash)

- (NSUInteger)selectedIndex {
    return [self.items indexOfObject:self.selectedItem];
}

@end

@interface RakutenAppDelegate(Calabash)

- (NSString *)calabash:(NSDictionary *)args;

@end

@implementation RakutenAppDelegate(Calabash)

- (NSString *)calabash:(NSDictionary *)args {
    NSString *cmd = args[@"command"];
    if ([cmd isEqualToString:@"get_class"]) {
        NSString *name = args[@"name"];
        if (!name) return @"error|argument 'name' is missing";
        NSObject *obj = objc_getClass(name.UTF8String);
        if (!obj) return @"error|class not found";
        uintptr_t ptr = (__bridge void *)obj;
        return [NSString stringWithFormat:@"ok|%@|%lx", name, ptr];
    }
    if ([cmd isEqualToString:@"get_property"]) {
        NSString *object = args[@"object"];
        if (!object) return @"error|argument 'object' is missing";
        uintptr_t ptr;
        sscanf(object.UTF8String, "%lx", &ptr);
        NSObject *target = (__bridge NSObject *)((void *)ptr);
        NSString *name = args[@"key"];
        if (!name) return @"error|argument 'name' is missing";
        @try {
            NSObject *value = [target valueForKey:name];
            if (!value) return [NSString stringWithFormat:@"ok|nil"];
            if ([value isKindOfClass:NSString.class]) return [NSString stringWithFormat:@"ok|string|%@", value];
            if ([value isKindOfClass:NSNumber.class]) {
                char const *type = "number";
                NSNumber *number = (NSNumber *)value;
                switch (CFNumberGetType((CFNumberRef)number)) {
                    case kCFNumberSInt8Type:
                    case kCFNumberSInt16Type:
                    case kCFNumberSInt32Type:
                    case kCFNumberSInt64Type:
                    case kCFNumberCharType:
                    case kCFNumberShortType:
                    case kCFNumberIntType:
                    case kCFNumberLongType:
                    case kCFNumberLongLongType:
                    case kCFNumberNSIntegerType:
                    case kCFNumberCFIndexType:
                        type = "int";
                        break;
                    case kCFNumberFloat32Type:
                    case kCFNumberFloat64Type:
                    case kCFNumberFloatType:
                    case kCFNumberDoubleType:
                    case kCFNumberCGFloatType:
                        type = "float";
                        break;
                    default:
                        break;
                }
                if (number == (void *)kCFBooleanFalse || number == (void *)kCFBooleanTrue) type = "bool";
                return [NSString stringWithFormat:@"ok|%s|%@", type, value];
            }
            static NSMutableArray *array;
            static dispatch_once_t once;
            dispatch_once(&once, ^{
                array = [NSMutableArray new];
            });
            [array addObject:value];
            ptr = (__bridge void *)value;
            return [NSString stringWithFormat:@"ok|%s|%@", class_getName(value.class), [NSString stringWithFormat:@"%lx", ptr]];
        } @catch (NSException *exception) {
            return [NSString stringWithFormat:@"error|%@", exception.reason];
        }
    }
    if ([cmd isEqualToString:@"set_property"]) {
        NSString *object = args[@"object"];
        if (!object) return @"error|argument 'object' is missing";
        uintptr_t ptr;
        sscanf(object.UTF8String, "%lx", &ptr);
        NSObject *target = (__bridge NSObject *)((void *)ptr);
        NSString *key = args[@"key"];
        if (!key) return @"error|argument 'key' is missing";
        NSObject *value = args[@"value"];
        if (!value) return @"error|argument 'value' is missing";
        @try {
            NSError *error;
            if (![target validateValue:&value forKey:key error:&error]) {
                return [NSString stringWithFormat:@"error|%@", error.description];
            }
            [target setValue:value forKey:key];
        } @catch (NSException *exception) {
            return [NSString stringWithFormat:@"error|%@", exception.reason];
        }
        return @"ok";
    }
    if ([cmd isEqualToString:@"to_s"]) {
        NSString *object = args[@"object"];
        if (!object) return @"error|argument 'object' is missing";
        uintptr_t ptr;
        sscanf(object.UTF8String, "%lx", &ptr);
        NSObject *target = (__bridge NSObject *)((void *)ptr);
        return [NSString stringWithFormat:@"ok|%@", target.description];
    }
    return @"error|unknown command";
}

@end