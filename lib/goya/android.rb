require 'calabash-android/cucumber'

Calabash::Android::UsageTracker.disable_usage_tracking

require 'goya'
require 'goya/android/bridge'
require 'goya/android/device'
require 'goya/android/v2'
require 'goya/android/view'
