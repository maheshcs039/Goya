require 'goya/query'

def Page(*trait)
  page_class = Class.new(Page)
  page_class.send(:define_method, :trait) {
    Query.new(*trait)
  }
  page_class
end

class Page
  attr_reader :trait
  class << self; attr_accessor :views end
  def initialize(*args, **filter)
    arg0 = args.shift
    @trait = arg0.is_a?(Query) ? arg0.filter(*args, **filter) : Query.new(arg0, *args, **filter)
    @views = []
  end
  def self.view(symbol, view)
    (@views ||= []) << view
    send(:define_method, symbol) { view }
  end
  def traits
    [trait].compact
  end
  def views
    self.class.views || []
  end
  def self.open?
    new.open?
  end
  def open?
    traits.each { |trait|
      return false if trait.empty?
    }
    views.each { |view|
      return false unless view.exist?
    }
    true
  end
  def self.wait(*args)
    new.wait(*args)
  end
  def wait(*args)
    traits.each { |trait|
      trait.wait(*args)
    }
    views.each { |view|
      view.wait(*args)
    }
    self
  end
  def wait_gone(*args)
    traits.each { |trait|
      trait.wait_empty(*args)
    }
    views.each { |view|
      view.wait_gone(*args)
    }
    self
  end
end
