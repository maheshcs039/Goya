class Device
  def self.ios?
    false
  end
  def self.android?
    true
  end
  def self.platform
    :android
  end
  def self.name
    ENV['XTC_DEVICE_NAME'] || `#{calabash.default_device.adb_command} shell getprop ro.product.name`.strip
  end
  def self.model
    nil
  end
  def self.os_version
    ENV['XTC_DEVICE_OS']
  end
  def self.identifier
    calabash.default_device.serial
  end
  def self.resolution
    match = `#{calabash.default_device.adb_command} shell dumpsys window | grep init=`.match(/init=(\d+x\d+)/)
    match && match[1]
  end
end
