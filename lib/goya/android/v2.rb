require 'calabash-android/operations'

module Calabash
  module V2
    include Android::Operations
    alias_method :do_not_expect_view, :check_element_does_not_exist
    alias_method :view_should_not_exist, :check_element_does_not_exist
    alias_method :expect_view, :check_element_exists
    alias_method :view_should_exist, :check_element_exists
    alias_method :wait_for_view, :wait_for_element_exists
    alias_method :wait_for_no_view, :wait_for_element_does_not_exist
    alias_method :tap, :touch
    alias_method :enter_text, :keyboard_enter_text
    def view_exists?(query)
      !query(query).empty?
    end
    def enter_text_in(query, text)
      touch(query)
      sleep(1)
      keyboard_enter_text(text)
      hide_soft_keyboard
    end
    def wait_for_animations(timeout = 15)
      # not implemented
      sleep(1)
    end
    def wait_for_network_indicator(timeout = 30)
      # not implemented
    end
  end
end

World(Calabash::V2)
