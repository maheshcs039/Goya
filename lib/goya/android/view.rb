require 'goya/view'

module Android
end

module Android::View
end

class View
  prepend Android::View
  def self.default_getter(symbol)
    "get#{symbol.to_s.to_camel}".intern
  end
  def self.default_bool_getter(symbol)
    "is#{symbol.to_s.to_camel}".intern
  end
  def self.default_setter(symbol)
    "set#{symbol.to_s.to_camel}".intern
  end
end

def Widget(class_name)
  view_class = Class.new(View)
  view_class.define_singleton_method(:class_name) { |name = nil|
    super(name || "android.widget.#{class_name}")
  }
  view_class
end

class TextView < Widget('TextView')
  register_as :text_view
  property :text
end

class Button < Widget('Button')
  register_as :button
end

class CheckBox < Widget('CheckBox')
  register_as :check_box
  bool_getter :checked
end

class EditText < Widget('EditText')
  register_as :edit_text
  property :text
end

class ImageButton < Widget('ImageButton')
  register_as :image_button
end

class ListView < Widget('ListView')
  register_as :list_view
end

class LinearLayout < Widget('LinearLayout')
  register_as :linear_layout
end

class ImageView < Widget('ImageView')
  register_as :image_view
end

class CheckedTextView < Widget('CheckedTextView')
  register_as :checked_text_view
end

class ProgressBar < Widget('ProgressBar')
  register_as :progress_bar
end

class ScrollView < Widget('ScrollView')
  register_as :scroll_view
end

class FrameLayout < Widget('FrameLayout')
  register_as :frame_layout
end

class RelativeLayout < Widget('RelativeLayout')
  register_as :relative_layout
end

class RatingBar < Widget('RatingBar')
  register_as :rating_bar
  property :rating
end
