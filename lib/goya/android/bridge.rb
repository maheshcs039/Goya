module Bridge
  class Package
    def self.Class(name)
      Class.new(self, name)
    end
    def self.const_missing(name)
      Class(name)
    end
  end
  def self.Package(name)
    package = ::Class.new(Package)
    package.define_singleton_method(:name) { name }
    package.define_singleton_method(:to_s) { name }
    package
  end
  class Context
    attr_reader :parent, :name, :type
    def initialize(name, type)
      @name, @type = name, type
    end
    def eval
      retval = Bridge.eval(to_s)
      return nil if !retval || retval == 'null' || @type == 'void'
      return retval == 'true' if @type == 'boolean'
      return retval.to_i if @type == 'int' || @type == 'short' || @type == 'long'
      retval
    end
    def method_missing(symbol, *args)
      name = symbol.to_s
      bang = name.end_with?('!')
      name.chop! if bang
      map = Bridge.mapping[@type]
      ctx = nil
      if map
        field = map.fields[name] if args.empty?
        if field
          ctx = Field.new(self, field.name, field.type)
        else
          method = map.methods[name] && map.methods[name].find { |m| m.check_args(args) }
          ctx = Method.new(self, method.name, method.type, *args) if method
        end
      end
      ctx ||= Method.new(self, name, nil, *args)
      bang ? ctx.eval : ctx
    end
  end
  class Class < Context
    def initialize(package, name)
      super(name, "#{package}.#{name}")
      @package = package
    end
    def to_s
      "#{@package}.#{@name}"
    end
  end
  class Field < Context
    def initialize(object, name, type)
      super(name, type)
      @object = object
    end
    def to_s
      "#{@object}.#{@name}"
    end
  end
  class Method < Context
    def initialize(object, name, type, *args)
      super(name, type)
      @object, @args = object, args
    end
    def to_s
      "#{@object}.#{@name}(#{@args.join(',')})"
    end
  end
  class Mapping
    class TypeMap
      attr_reader :name, :type, :args
      def initialize(name, type, *args)
        @name, @type, @args = name, type, args
      end
      def check_args(args)
        @args.size == args.size
      end
    end
    class Type
      attr_reader :type, :fields, :methods
      def initialize(type, lines)
        @type = type
        @fields = {}
        @methods = {}
        lines.each { |line|
          line.match(/^(.+) (\w+) -> (\w+)$/) { |m|
            @fields[m[2]] = TypeMap.new(m[3], m[1])
          }
          line.match(/^(?:(?:\d+):(?:\d+):)?(.+) (\w+)\((.*)\) -> (\w+)$/) { |m|
            (@methods[m[2]] ||= []) << TypeMap.new(m[4], m[1], *m[3].split(','))
          }
        }
      end
    end
    def initialize(path)
      lines = IO.readlines(path)
      @types = {}
      src_type = dst_type = nil
      type_lines = []
      lines.each { |line|
        line.strip!
        if line.end_with?(':')
          @types[src_type] = Type.new(dst_type, type_lines) if src_type
          src_type, dst_type = *line.chop.split(' -> ')
          type_lines = []
        else
          type_lines << line
        end
      }
      @types[src_type] = Type.new(dst_type, type_lines) if src_type
    end
    def [](type)
      @types[type]
    end
  end
  def self.mapping
    @@mapping ||= Mapping.new
  end
  def self.load_mapping(path)
    @@mapping = Mapping.new(path)
  end
  def self.eval(expr)
    calabash.backdoor('bridge', expr)
  end
end
