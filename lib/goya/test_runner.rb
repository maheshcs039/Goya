require 'etc'
require 'socket'
require 'concurrent'
require 'goya/kitsune'

Kitsune.check_version!

class TestRunner
  def self.run(class_name, args, table = nil)
    @file_cache ||= {}
    set_trace_func proc { |event, file, line, _id, _binding, class_sym|
      if event == 'line' && class_sym.to_s == class_name
        lines = @file_cache[file] ||= IO.readlines(file)
        source = lines[line - 1]
        match = source.match(/##(.+)/)
        step = match && match[1].strip
        Kernel.puts "- #{step}" if step
      end
    }
    args = (args || '').split(',').map(&:strip)
    args << table.raw if table
    begin
      test = Kernel.const_get(class_name).new
      test.setup if test.respond_to?(:setup)
      test.run(*args)
    ensure
      test.teardown if test.respond_to?(:teardown)
      set_trace_func(nil)
    end
  end
end

class TestRunner::Log
  @log = ''
  def self.write(string)
    @log += string
  end
  def self.flush
    if $test_session
      $test_session.message(@log)
      @log = ''
    end
  end
end

class << $stdout
  def write(string)
    super
    TestRunner::Log.write(string)
  end
end

class << $stderr
  def write(string)
    super
    TestRunner::Log.write(string)
  end
end

class TestRunner::Formatter
  def initialize(config)
    config.on_event :before_test_case, &method(:on_before_test_case)
    config.on_event :after_test_case, &method(:on_after_test_case)
    config.on_event :before_test_step, &method(:on_before_test_step)
    config.on_event :after_test_step, &method(:on_after_test_step)
    config.on_event :finished_testing, &method(:on_finished_testing)
  end
  def on_before_test_case(event)
    # test_case = event.test_case
  end
  def on_after_test_case(event)
    # test_case = event.test_case
  end
  def on_before_test_step(event)
    @watchdog = Concurrent::TimerTask.new(execution_interval: 600) {
      Thread.main.raise Timeout::Error, 'Timeout executing scenario'
    }
    @watchdog.execute
  end
  def on_after_test_step(event)
    @watchdog.shutdown
  end
  def on_finished_testing(event)
    TestRunner::Log.flush
    if $test_session
      puts "Session saved at #{$test_session.url}".yellow
      $test_session.finish
      $test_session = nil
    end
  end
  def puts(message)
  end
  def embed(src, mime_type, label)
  end
end

AfterConfiguration { |config|
  $tags = config.tag_expressions.map { |tag| tag[1..-1] }
  class << config
    def formatter_factories
      super + [(yield TestRunner::Formatter, nil, nil)]
    end
  end
  Kernel.puts "Server offline: #{Kitsune::URL}".red if !Kitsune.online?
}

Before { |scenario|
  cleanup = Proc.new {
    TestRunner::Log.flush
    if @test_run
      @test_run.finish('aborted')
      @test_run = nil
    end
    if $test_session
      puts "Session results at #{$test_session.url}".yellow
      $test_session.finish
      $test_session = nil
    end
  }
  at_exit {
    cleanup.call
  }
  trap('INT') {
    Kernel.puts 'Interrupted'.red
    cleanup.call
    exit!(1)
  }
  world = CalabashProxy.new(self)
  Kernel.send(:define_method, :calabash) { world }
  class << self
    def screenshot(arg = nil)
      return super() if Goya.screenshot_mode == :disabled
      name = arg if arg.is_a?(String)
      name = arg[:name] if arg.is_a?(Hash)
      if name.nil?
        m = caller(1, 1).first.match(%r{.*\/(.+):(\d+)})
        name = "#{m[1]} line #{m[2]}"
      end
      path = super()
      @test_run.screenshot(name, path) rescue nil if @test_run && File.exist?(path)
      path
    end
    def request_resource(name)
      @test_run.nil? || @test_run.create_resource(name)
    end
    def release_resource(name)
      @test_run && @test_run.delete_resource(name)
    end
  end
  if Kitsune.online?
    if $test_session.nil?
      project = Kitsune::Project.find_or_create(name: Goya.project_name)
      device_params = {
        name: Device.name,
        platform: Device.platform,
        os_version: Device.os_version,
        resolution: Device.resolution
      }
      device = Kitsune::Device.find_or_create(device_params)
      host = Socket.gethostname rescue ENV['HOSTNAME']
      env = ENV.to_h.filter('USER', 'HOSTNAME', 'RUBY_VERSION').merge(GOYA_VERSION: Goya::VERSION)
      session_params = {
        project_id: project.id,
        name: ENV['SESSION_NAME'],
        host: host,
        user: ENV['USER'],
        device_id: device.id,
        tags: $tags,
        environment: env
      }
      $test_session = Kitsune::TestSession.create(session_params)
      if !$test_session.errors.empty?
        Kernel.puts 'Failed creating session:'.red
        $test_session.print_errors
      else
        Kernel.puts "Session created at #{$test_session.url}".yellow
      end
    end
    if $test_session.valid?
      TestRunner::Log.flush
      test_case_params = {
        project_id: $test_session.project_id,
        feature: scenario.feature.name,
        tags: scenario.tags.map { |tag| tag.name[1..-1] }
      }
      if scenario.outline?
        match = scenario.name.match(/(.*), Examples \(#(\d+)\)$/)
        test_case_params[:name] = match[1]
        test_case_params[:variant] = match[2].to_i
        test_case_params[:test_data] = scenario.source.last.instance_variable_get('@data')
      else
        test_case_params[:name] = scenario.name
      end
      test_case = Kitsune::TestCase.find_or_create(test_case_params)
      test_run_params = {
        project_id: test_case.project_id,
        test_case_id: test_case.id,
        test_session_id: $test_session.id,
        status: 'running'
      }
      @test_run = Kitsune::TestRun.create(test_run_params)
      if !@test_run.errors.empty?
        Kernel.puts 'Failed creating test run:'.red
        @test_run.print_errors
        @test_run = nil
      end
    end
  end
  @started = Time.now
}

After { |scenario|
  if @started
    t = (Time.now - @started).to_i
    str = "#{t % 60}s"
    t /= 60
    str = "#{t}m#{str}" if t >= 1
    str = t < 5 ? str.green : (t < 10 ? str.yellow : str.red)
    puts 'Duration:'.yellow + ' ' + str
  end
  screenshot('Failed') unless scenario.passed? || xamarin_test_cloud?
  TestRunner::Log.flush
  if @test_run
    @test_run.finish(scenario.passed? ? 'passed' : 'failed')
    @test_run = nil
  end
}

AfterStep { |_result, step|
  sleep(0.5)
  screenshot(step.name) if Goya.screenshot_mode == :auto
}

Given(/^([A-Z]\w+)(?:\((.*)\))?$/, :run, on: -> { TestRunner })
