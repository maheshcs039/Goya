class Formatter
  def initialize(runtime, io, options)
    @io = io
  end
  def before_feature(feature)
    @io.puts "Feature #{feature}"
  end
  def after_feature_element(feature_element)
    status = feature_element.status
    @io.puts "Status: #{status == :passed ? status.to_s.green : status.to_s.red}"
  end
  def exception(exception, status)
    @io.puts exception.message
    @io.puts exception.backtrace
  end
end
