require 'active_resource'
require 'versionomy'
require 'rest-client'
require 'yaml'

ActiveSupport::Deprecation.silenced = true

class Kitsune
  URL = ENV['KITSUNE_URL'] || 'https://vats.rakuten-it.com'
  def self.welcome
    @welcome ||= Welcome.first rescue nil
  end
  def self.online?
    welcome.online? rescue false
  end
  def self.min_version
    welcome.min_goya_version rescue nil
  end
  def self.latest_version?
    Versionomy.parse(Goya::VERSION) >= Versionomy.parse(min_version)
  end
  def self.check_version
    !online? || latest_version?
  end
  def self.check_version!
    return if check_version
    puts "Goya gem has been updated, please run 'bundle update'".red
    puts "Installed version: #{Goya::VERSION}, minimum required: #{min_version}".red
    exit(1)
  end
  class Resource < ActiveResource::Base
    self.site = URL
    self.ssl_options = { verify_mode: OpenSSL::SSL::VERIFY_NONE } if URL.start_with?('https')
    self.user = 'goya'
    self.password = 'bVQfRQ4rmNmH'
    def self.find_or_create(**params)
      params = params.reject { |k, v| v.nil? }
      where(**params).first || create(**params)
    end
    def initialize(attributes = {}, persisted = false)
      super(attributes.has_key?("error") ? parse_errors(attributes) : attributes, persisted)
    end
    def print_errors
      errors.full_messages.each { |msg| puts "- #{msg.red}" }
    end
    def url
      "#{self.class.site}#{self.class.collection_name}/#{id}"
    end
  end
  class Welcome < Resource
    self.collection_name = "welcome"
  end
  class Message < Resource
  end
  class Device < Resource
  end
  class TestCase < Resource
  end
  class TestResource < Resource
    # belongs_to :test_run, class_name: 'Kitsune::TestRun'
    def object
      YAML.load(value)
    end
  end
  class TestSession < Resource
    # belongs_to :project, class_name: 'Kitsune::Project'
    def message(text)
      Message.create(messenger_id: id, messenger_type: 'TestSession', text: text)
    end
    def finish
      self.finished_at = Time.now
      save
    end
  end
  class TestRun < Resource
    # has_many :messages, class_name: 'Kitsune::Message'
  end
  class Project < Resource
    has_many :test_cases, class_name: 'Kitsune::TestCase'
    # has_many :test_sessions, class_name: 'Kitsune::TestSession'
    def upload_resource(name, resource)
      TestResource.create(project_id: id, name: name, class_name: resource.class.to_s, value: resource.to_yaml)
    end
  end
  class Screenshot < Resource
    belongs_to :test_run, class_name: 'Kitsune::TestRun'
  end
  class TestRun < Resource
    belongs_to :test_case, class_name: 'Kitsune::TestCase'
    belongs_to :test_session, class_name: 'Kitsune::TestSession'
    has_many :screenshots, class_name: 'Kitsune::Screenshot'
    def message(text)
      Message.create(messenger_id: id, messenger_type: 'TestRun', text: text)
    end
    def screenshot(title, path)
      screenshot = Screenshot.create(test_case_id: test_case_id, test_run_id: id, title: title)
      resource = RestClient::Resource.new(screenshot.url, user: self.class.user, password: self.class.password, verify_ssl: OpenSSL::SSL::VERIFY_NONE)
      begin
        resource.put('screenshot[image]' => File.new(path))
      rescue RestClient::Found
      rescue Exception => e
        screenshot.error = e.message
      end
    end
    def create_resource(name, object = nil)
      return nil if Kitsune::TestResource.where(project_id: project_id, name: name).first
      Kitsune::TestResource.create(project_id: project_id, test_run_id: id, name: name, value: object && object.to_yaml)
    end
    def delete_resource(name)
      resource = Kitsune::TestResource.where(project_id: project_id, test_run_id: id, name: name).first
      resource.destroy if resource
    end
    def resource(name, reserve = false)
      resource = Kitsune::TestResource.where(project_id: project_id, test_run_id: nil, name: name).first
      return nil if !resource
      resource.update_attribute(:test_run_id, id) if reserve
      resource
    end
    def release_resource(name)
      resource = Kitsune::TestResource.where(project_id: project_id, test_run_id: id, name: name).first
      resource.update_attribute(:test_run_id, nil) if resource
    end
    def finish(status)
      self.status = status
      self.finished_at = Time.now
      save
    end
  end
  class TestCase < Resource
    belongs_to :project, class_name: 'Kitsune::Project'
    has_many :test_runs, class_name: 'Kitsune::TestRun'
    def self.find_or_create(**params)
      params = params.reject { |k, v| v.nil? }
      find_params = params.slice(:project_id, :feature, :name, :variant, :test_data)
      TestCase.where(find_params).first || TestCase.create(params)
    end
  end
end
