class CalabashProxy < BasicObject
  DEBUG = ::ENV['DEBUG'] == '1'
  def initialize(world)
    @world = world
  end
  def method_missing(method, *args, **hash, &block)
    if DEBUG
      message = method.to_s
      args.map(&:inspect).concat(hash.map { |k, v| "#{k}: #{v.inspect}"}).tap { |args|
        message += "(#{args.join(', ')})" if !args.empty?
      }
      $stdout.puts "DEBUG: #{message}".yellow
    end
    retval = hash.empty? ? @world.send(method, *args, &block) : @world.send(method, *args, **hash, &block)
    $stdout.puts "DEBUG: #{retval.inspect}".yellow if DEBUG
    retval
  end
end

class String
  # @return [String] string converted to camel case style, e.g. "to_camel" becomes "ToCamel"
  def to_camel
    split('_').collect(&:capitalize).join
  end
  # @return [Fixnum] number representation of the string
  # @return nil if there are no digit characters in the string
  def to_number
    digits = scan(/\d+/)
    digits.empty? ? nil : digits.join.to_i
  end
end

class FalseClass
  def to_i; 0; end
end

class TrueClass
  def to_i; 1; end
end

class Hash
  def filter(*keys)
    select { |k, v| keys.include?(k) }
  end
end
