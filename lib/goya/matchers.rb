require 'rspec/expectations'

RSpec::Matchers.define :appear do
  match do |view|
    begin
      view.wait
    rescue RuntimeError
      false
    end
  end
  failure_message do |view|
    "expected that #{view} would appear"
  end
end

RSpec::Matchers.define :disappear do
  match do |view|
    begin
      view.wait_gone
    rescue RuntimeError
      false
    end
  end
  failure_message do |view|
    "expected that #{view} would disappear"
  end
end
