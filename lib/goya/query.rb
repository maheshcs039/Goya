# Represents Calabash query.
#
# For more information on Calabash query format, see:
# https://developer.xamarin.com/guides/testcloud/calabash/calabash-query-syntax/
class Query
  class Filter < Hash
    @bool_is_int = false
    class << self; attr_accessor :bool_is_int; end
    def to_s
      map { |key, value|
        value = "'#{value}'" if value.is_a?(String)
        value = value.to_i if Filter.bool_is_int && (value.is_a?(FalseClass) || value.is_a?(TrueClass))
        "#{key}:#{value}"
      }
    end
  end
  attr_reader :components
  def self.all(*args)
    new(:all, *args)
  end
  def initialize(*args, **filter)
    @components = args.compact
    @components << '*' if @components.empty?
    predicate = filter.delete(:predicate)
    @components << Filter[filter]
    @components << "{#{predicate}}" if predicate
  end
  def to_s
    @components.map(&:to_s).join(' ').strip
  end
  def all
    @components.first.to_sym == :all ? self : Query.new(:all, self)
  end
  def all!
    @components.unshift(:all) if @components.first.to_sym != :all
    self
  end
  def filter(*args)
    args.empty? ? self : Query.new(self, *args)
  end
  def descendant(*args)
    Query.new(self, :descendant, *args)
  end
  alias_method :desc, :descendant
  def child(*args)
    Query.new(self, :child, *args)
  end
  def parent(*args)
    Query.new(self, :parent, *args)
  end
  def sibling(*args)
    Query.new(self, :sibling, *args)
  end
  def predicate(expr)
    Query.new(self, "{#{expr}}")
  end
  alias_method :pred, :predicate
  def run(*args)
    calabash.query(to_s, *args)
  end
  def to_a
    run
  end
  def flash
    calabash.flash(to_s)
  end
  def touch(**options)
    calabash.touch(to_s, **options)
  end
  def size
    run.size
  end
  def empty?
    run.empty?
  end
  def first(*args)
    run(*args).first
  end
  def last(*args)
    run(*args).last
  end
  def [](index)
    run[index]
  end
  def wait(*args)
    if block_given?
      calabash.wait_for(*args) {
        next true unless empty?
        yield && false if block_given?
      }
    else
      calabash.wait_for_view(to_s, *args)
    end
  end
  def wait_empty(*args)
    if block_given?
      calabash.wait_for(*args) {
        next true if empty?
        yield && false if block_given?
      }
    else
      calabash.wait_for_no_view(to_s, *args)
    end
  end
end
