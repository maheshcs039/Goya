require 'calabash-cucumber'

module Calabash
  module V2
    include Cucumber::Operations
    alias_method :do_not_expect_view, :check_element_does_not_exist
    alias_method :view_should_not_exist, :check_element_does_not_exist
    alias_method :expect_view, :check_element_exists
    alias_method :view_should_exist, :check_element_exists
    alias_method :wait_for_view, :wait_for_element_exists
    alias_method :wait_for_no_view, :wait_for_element_does_not_exist
    alias_method :tap, :touch
    alias_method :enter_text, :keyboard_enter_text
    def view_exists?(query)
      !query(query).empty?
    end
    def enter_text_in(query, text)
      touch(query)
      wait_for_keyboard
      keyboard_enter_text(text)
    end
    def wait_for_animations(timeout = 15)
      # wait_for_none_animating(timeout: timeout)
      sleep(1)
    end
    def wait_for_network_indicator(timeout = 30)
      wait_for_no_network_indicator(timeout: timeout)
    end
  end
end

World(Calabash::V2)
