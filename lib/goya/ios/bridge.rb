module Bridge
  def self.request(cmd, **args)
    hash = {command: cmd}
    hash.merge!(args)
    resp = calabash.backdoor('calabash:', Hash[hash.map { |k, v| [k.to_s, v] }]).split('|')
    raise resp[1] if resp[0] != 'ok'
    resp.shift
    resp
  end
  def self.request_eval(*args)
    resp = request(*args)
    return nil if resp.empty?
    class_name = resp.first
    return nil if class_name == 'nil'
    return resp[1] if class_name == 'string'
    return resp[1] == '1' if class_name == 'bool'
    return resp[1].to_i if class_name == 'int'
    return resp[1].to_f if class_name == 'float'
    #return Array.new(*resp) if class_name == '__NSArrayI' || class_name == '__NSArrayM'
    Object.new(*resp)
  end
  class Object
    def initialize(class_name, ptr)
      @class_name, @ptr = class_name, ptr
    end
    def method_missing(name, *args)
      name = name.to_s
      if name.end_with?('=')
        value = args.first
        Bridge.request('set_property', object: @ptr, key: name.chop, value: value)
        return value
      end
      Bridge.request_eval('get_property', object: @ptr, key: name)
    end
  end
  class Class < Object
  end
  class Array < Object
  end
  def self.const_missing(name)
    Class.new(*request('get_class', name: name))
  end
end
