# Alert popup
class Alert < View('_UIAlertControllerView')
  class Button < View('_UIAlertControllerActionView')
    def title
      label
    end
  end
  # Alert title
  def title
    label
  end
  # Alert message
  def message
    query.descendant(view: '_UIInterfaceActionGroupHeaderScrollView').descendant('label').last(:text)
  end
  # Alert buttons
  def buttons
    buttons_query = query.descendant(view: '_UIAlertControllerActionView')
    buttons_query.to_a.map.with_index { |q, i| Button.new(buttons_query, index: i) }
  end
end
