class Device
  def self.ios?
    true
  end
  def self.android?
    false
  end
  def self.platform
    :ios
  end
  def self.name
    ENV['XTC_DEVICE_NAME'] || calabash.default_device && calabash.default_device.device_name
  end
  def self.model
    calabash.default_device && calabash.default_device.model_identifier
  end
  def self.os_version
    ENV['XTC_DEVICE_OS'] || calabash.default_device && calabash.default_device.ios_version
  end
  def self.identifier
  end
  def self.resolution
    hash = calabash.default_device && calabash.default_device.screen_dimensions
    hash && "#{hash[:width]}x#{hash[:height]}"
  end
end
