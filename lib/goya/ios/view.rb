Query::Filter.bool_is_int = true

module IOS
end

module IOS::View
  def flick(x: 0, y: 0, offset: {x: 0, y: 0})
    calabash.flick(query.to_s, {x: x, y: y}, offset: offset)
  end
  def flick_until_exist(direction)
    wait {
      calabash.swipe(direction)
    }
  end
  def flick_and_tap(direction)
    flick_until_exist(direction)
    tap!
  end
end

class View
  prepend IOS::View
  def self.class_name(name = nil)
    return '*' if !name
    name.start_with?('_') ? "view:'#{name}'" : name
  end
  def self.default_getter(symbol)
    symbol.to_s.to_camel.intern
  end
  def self.default_bool_getter(symbol)
    "is#{symbol.to_s.to_camel}".intern
  end
  def self.default_setter(symbol)
    "set#{symbol.to_s.to_camel}".intern
  end
  def label
    query.last(:accessibilityLabel)
  end
end

class ScrollView < View('UIScrollView')
  register_as :scroll_view
end

class Label < View('UILabel')
  register_as :label
  property :text
end

class Button < View('UIButton')
  register_as :button
  getter :title, :currentTitle
end

class TextField < View('UITextField')
  register_as :text_field
  property :text
  def clear_button
    View.new(query.child('UIButton'))
  end
end

class SegmentedControl < View('UISegmentedControl')
  register_as :segmented_control
end

class SegmentLabel < View('UISegmentLabel')
  register_as :segmented_label
end

class NavigationBar < View('UINavigationBar')
  register_as :navigation_bar
end

class TabBar < View('UITabBar')
  register_as :tab_bar
end

class TabBarButton < View('UITabBarButton')
  register_as :tab_bar_button
end

class NavigationButton < View('UINavigationButton')
  register_as :navigation_button
end

class NavigationBackButton < View('_UINavigationBarBackIndicatorView')
  register_as :navigation_back_button
end

class NavigationItemBackButton < View('UINavigationItemButtonView')
  register_as :navigation_item_back_button
end

class TableViewCellContentView < View('UITableViewCellContentView')
  register_as :table_view_cell_content_view
end

class Switch < View('UISwitch')
  register_as :switch
  bool_property :on
end

class ImageView < View('UIImageView')
  register_as :image_view
end

class TableView < View('UITableView')
  register_as :table_view
  class Cell < View('UITableViewCell')
    property :text
    attr_reader :row, :section
    def initialize(*args, **filter)
      @row = filter.delete(:row)
      @section = filter.delete(:section)
      @mark = filter[:marked] || filter[:text]
      super(*args, **filter)
    end
    def checked?
      accessory = query.run(:accessoryView).first
      accessory && accessory['id'] == 'TableRowCheckRed'
    end
    def switch
      Switch.new(query.desc('UISwitch'))
    end
    def scroll_to
      if @mark
        calabash.scroll_to_row_with_mark(@mark)
      elsif @row
        calabash.scroll_to_cell(row: @row, section: @section || 0)
      end
      self
    end
  end
  def cell(**filter)
    Cell.new(query.desc('UITableViewCell', **filter))
  end
  def self.cell(symbol, *query)
    send(:define_method, symbol) {
      name = "@#{symbol}"
      instance_variable_defined?(name) ? instance_variable_get(name) : instance_variable_set(name, Cell.new(*query))
    }
  end
end
