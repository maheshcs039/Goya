require 'goya/view'

class ActionSheet < View
  def initialize
    super("view:'_UIAlertControllerView'")
  end
  def title
    calabash.uia('UIATarget.localTarget().frontMostApp().actionSheet().label()')
  end
  def buttons
    calabash.query("view:'_UIAlertControllerActionView'").map { |res| res['label'] }
  end
  def tap_button(index)
    calabash.touch("view:'_UIAlertControllerActionView' index:#{index}")
  end
end
