require 'goya/view'

class WebView < View
  register_as :web_view
  def self.class_name(_name = nil)
    'webView'
  end
  def wait(*args)
    wait_page_load
    super(*args)
  end
  def wait_page_load
    calabash.wait_for_network_indicator
  end
  def js(js)
    return calabash.evaluate_javascript(query.to_s, "return #{js};") if Device.android?
    query.first(stringByEvaluatingJavaScriptFromString: js)
  end
  def html
    js('document.body.innerHTML')
  end
end
