require 'goya/query'

# Parameterized View class with its class_name set to return the provided argument.
def View(class_name)
  view_class = Class.new(View)
  view_class.define_singleton_method(:class_name) { |name = nil|
    super(name || class_name)
  }
  view_class
end

# Base class representing a UI element.
# For full screen container elements identified by their contents, consider using Page class instead.
class View
  # Point class
  class Point
    # @return [Float] x coordinate
    attr_reader :x
    # @return [Float] y coordinate
    attr_reader :y
    def initialize(x, y)
      @x, @y = x, y
    end
  end
  # Rectangle class
  class Rect
    # @return [Float] left coordinate
    attr_reader :x
    # @return [Float] top coordinate
    attr_reader :y
    # @return [Float] height
    attr_reader :width
    # @return [Float] width
    attr_reader :height
    # @return [Point] center coordinate
    attr_reader :center
    def initialize(hash)
      @x, @y, @width, @height = *%w(x y width height).map { |key| hash[key].to_f }
      @center = Point.new(hash['center_x'].to_f, hash['center_y'].to_f)
    end
  end
  # @return [Query] Query backing this view.
  attr_reader :query
  # @return [View, String, Symbol] View or page (instance, constant or class name) that the application will transition to when this view is tapped.
  attr_accessor :tap_target
  # Creates this view from a query. The query should normally resolve to empty or single result.
  # @param [Query, String, nil] args either Query instance, native class name, or empty
  # @param [Hash] filter query filter
  # @example
  #   view = View.new('MyClass', id: 'my_control')
  def initialize(*args, **filter)
    arg0 = args.shift
    @tap_target = filter.delete(:tap_target)
    @query = arg0.is_a?(Query) ? arg0.filter(*args, **filter) : Query.new(self.class.class_name(arg0), *args, **filter)
  end
  # Alternate construction method for representing element that might be hidden or off screen.
  # Using this method effectively adds the "all" prefix in the Calabash query.
  def self.all(*args)
    view = new(*args)
    view.query.all!
    view
  end
  # Native class name that this class represents.
  def self.class_name(name = nil)
    name || '*'
  end
  # Checks if a view of this class is visible.
  def self.exist?
    new.exist?
  end
  # Waits for a view of this class to become visible.
  # @param [Hash] args wait options: _timeout_, _timeout_message_
  # @yield block to execute until a view becomes visible, e.g. scroll
  def self.wait(*args, &block)
    new.wait(*args, &block)
  end
  def parent(*args)
    View.new(query.parent(*args))
  end
  def descendant(*args)
    View.new(query.descendant(*args))
  end
  alias_method :desc, :descendant
  def child(*args)
    View.new(query.child(*args))
  end
  def sibling(*args)
    View.new(query.sibling(*args))
  end
  # Checks if this view is visible.
  def exist?
    !query.empty?
  end
  alias_method :visible?, :exist?
  # @deprecated Use `expect(view).to exist` instead.
  # Fails with error message unless the view exists.
  def exist!(fail_message = nil)
    expect(self).to exist, fail_message || "View does not exist: #{query}"
  end
  # @deprecated Use `expect(view).to_not exist` instead.
  # Fails with error message if the view exists.
  def not_exist!(fail_message = nil)
    expect(self).to_not exist, fail_message || "View should not exist: #{query}"
  end
  # Waits for this view to become visible.
  # @param [Hash] args wait options: _timeout_, _timeout_message_
  # @yield block to execute until a view becomes visible, e.g. scroll
  def wait(*args, &block)
    query.wait(*args, &block)
    self
  end
  # Waits for this view to become invisible.
  # @param [Hash] args wait options: _timeout_, _timeout_message_
  # @yield block to execute until a view becomes invisible, e.g. scroll
  def wait_gone(*args, &block)
    query.wait_empty(*args, &block)
    self
  end
  # @return [Hash] hash containing this view properties (empty if the view is not visible)
  def to_h
    query.first || {}
  end
  # @deprecated Use {#to_h} instead.
  # @param [String] hash key
  def [](key)
    to_h[key]
  end
  # @return [Rect] rectangle defining this view, or nil when this view is not visible
  def rect
    rect = to_h['rect']
    rect && Rect.new(rect)
  end
  def enter_text(text = "\n")
    wait
    calabash.enter_text_in(query.to_s, text)
  end
  # Performs scroll gesture on the view.
  # @param direction [:up, :down] direction of the scroll.
  def scroll(direction)
    calabash.scroll(query.to_s, direction)
  end
  # Performs swipe gesture until this view becomes visible.
  # @param direction [:up, :down] direction of the swipe.
  def scroll_to(direction = :up)
    wait {
      calabash.swipe(direction)
    }
  end
  # Flashes this view, then taps it.
  def flash_tap
    flash && tap!
  end
  # Flashes this view.
  def flash
    query.flash
  end
  # Taps this view.
  def tap
    wait
    calabash.wait_for_animations
    query.touch
    calabash.wait_for_animations
    return self if tap_target.nil?
    if tap_target.is_a?(Array)
      args = tap_target[1..-1]
      target = Kernel.const_get(tap_target.first)
      target.new(*args).wait
    else
      target = tap_target.is_a?(String) || tap_target.is_a?(Symbol) ? Kernel.const_get(tap_target) : tap_target
      target.wait
    end
  end
  alias_method :tap!, :tap
  # Taps this view if it's visible.
  def tap_if_exist
    exist? && tap!
  end
  class << self
    def ab_test(*variants)
      klass = self
      variants = ["#{self}A", "#{self}B"] if variants.empty?
      send(:define_singleton_method, :new) { |*args|
        return super(*args) if binding.receiver != klass
        views = variants.map { |var| Kernel.const_get(var).new(*args) }
        variant = nil
        calabash.wait_for {
          variant = views.find(&:exist?)
        }
        raise "Neither variant exists: #{variants.join(', ')}" if !variant
        variant
      }
    end
    protected
    def register_as(symbol)
      klass = self
      Class.send(:define_method, symbol) { |name, *query|
        send(:define_method, name) {
          name = name.to_s
          name = "@#{name}" unless name.start_with?('@')
          instance_variable_defined?(name) ? instance_variable_get(name) : instance_variable_set(name, klass.new(*query))
        }
      }
    end
    def property(symbol)
      getter(symbol)
      setter(symbol)
    end
    def bool_property(symbol)
      bool_getter(symbol)
      setter(symbol)
    end
    def getter(symbol, *path)
      path = [default_getter(symbol)] if path.empty?
      send(:define_method, symbol) { get_property(*path) }
    end
    def bool_getter(symbol, *path)
      path = [default_bool_getter(symbol)] if path.empty?
      send(:define_method, "#{symbol}?".intern) { [1, true].include?(get_property(*path)) }
    end
    def setter(symbol, *path)
      path = [default_setter(symbol)] if path.empty?
      key = path.pop
      send(:define_method, "#{symbol}=".intern) { |value|
        set_property(*path, key => value)
      }
    end
    def default_getter(symbol)
      symbol
    end
    def default_bool_getter(symbol)
      symbol
    end
    def default_setter(symbol)
      symbol
    end
  end
  protected
  def get_property(*path, **key_value)
    raise ArgumentError unless key_value.empty?
    query.first(*path)
  end
  def set_property(*path, **key_value)
    raise ArgumentError if key_value.size != 1
    key, value = *key_value.first
    value = value.to_i if value.is_a?(FalseClass) || value.is_a?(TrueClass)
    key_value = {key => value}
    query.first(*path, **key_value)
  end
  register_as :view
end
