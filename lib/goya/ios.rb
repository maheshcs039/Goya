require 'calabash-cucumber'

Calabash::Cucumber::UsageTracker.disable_usage_tracking

require 'goya'
require 'goya/ios/action_sheet'
require 'goya/ios/alert'
require 'goya/ios/bridge'
require 'goya/ios/device'
require 'goya/ios/v2'
require 'goya/ios/view'
