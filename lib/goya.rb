require 'goya/version'

module Goya
  def self.project_name
    @project_name || File.basename(Dir.getwd).capitalize
  end
  def self.project_name=(name)
    @project_name = name
  end
  def self.disable_screenshots
    screenshot_mode = :disabled
  end
  def self.screenshots_disabled?
    screenshot_mode != :disabled
  end
  def self.screenshot_mode
    @screenshot_mode || :auto
  end
  # @param screenshot_mode [:disabled, :manual, :auto]
  def self.screenshot_mode=(screenshot_mode)
    @screenshot_mode = screenshot_mode
  end
  def self.screenshot_path=(path)
    path += '/' if !path.end_with?('/')
    FileUtils.mkdir_p(path) rescue nil
    ENV['SCREENSHOT_PATH'] = path
  end
  def self.delete_screenshots
    path = ENV['SCREENSHOT_PATH']
    return if !path || !Dir.exist?(path)
    Dir.new(path).each { |f|
      name = path + f
      File.delete(name) if f =~ /^screenshot_\d+\.png$/
    }
  end
end

require 'goya/kitsune'
require 'goya/matchers'
require 'goya/page'
require 'goya/query'
require 'goya/utility'
require 'goya/view'
require 'goya/web_view'
